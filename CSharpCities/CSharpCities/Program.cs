﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CSharpCities
{
    public static class CitiesExtensions
    {
        public static string SkipWrongChar(this string str)
        {
            var fchar = str.ToLower().Last();

            if (fchar == 'ы' || fchar == 'ь' || fchar == 'ъ')
                return str.Remove(str.Length - 1,1);
            return str;
        }
    }

    class Program
    {
        private static string Path = $"{Directory.GetCurrentDirectory()}/input.txt";


        static async Task Main(string[] args)
        {
            var difference = new Dictionary<char, int>();
            var cities = (await File.ReadAllLinesAsync(Path)).Select(x => x.ToLower()).Select(x => x.SkipWrongChar())
                .ToList();
            cities.ForEach(x =>
            {
                var first = x[0];
                var last = x[x.Length - 1];
                if (first == last) return;
                if (!difference.TryAdd(first, 1))
                    difference[first]++;
                if (!difference.TryAdd(last, -1))
                    difference[last]--;
            });
            var containsPlusOne = false;
            var containsMinuseOne = false;
            var error = false;

            foreach (var item in difference)
            {
                if (item.Value == 0) continue;
                if (item.Value == 1 && !containsPlusOne)
                {
                    containsPlusOne = true;
                    continue;
                }

                if (item.Value == -1 && !containsMinuseOne)
                {
                    containsMinuseOne = true;
                    continue;
                }

                error = true;
                break;
            }

            Console.WriteLine(error ? "Error" : "OK");
        }
    }
}