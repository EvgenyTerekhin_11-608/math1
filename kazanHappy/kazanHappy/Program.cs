﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace kazanHappy
{
    class Program
    {
        private static long[] previous { get; set; }
        private static long[] current { get; set; }
        private static long[] weigths { get; set; }

        private static int AmountOfNumbers = 8;

        static bool CanBeDividedInHalf(long number)
        {
            weigths = new long[AmountOfNumbers];
            long sum = 0;
            for (var i = AmountOfNumbers - 1; i >= 0; i--)
            {
                var lastNumber = number % 10;
                sum += lastNumber;
                weigths[i] = lastNumber;
                number /= 10;
            }


            if (sum % 2 == 1) return false;
            var halfSum = sum / 2;
            previous = new long[halfSum + 1];
            current = new long[halfSum + 1];
            for (var i = 0; i < weigths.Length; i++)
            {
                for (var ii = weigths[i]; ii <= halfSum; ii++)
                {
                    current[ii] = Math.Max(previous[ii], previous[ii - weigths[i]] + weigths[i]);
                    if (current[ii] == halfSum)
                        return true;
                }

                for (int ii = 0; ii <= halfSum; ii++)
                    previous[ii] = current[ii];
            }

            return previous[halfSum] == halfSum;
        }

        public class LongComparer : IEqualityComparer<long>
        {
            public bool Equals(long x, long y)
            {
                var xStr = x.ToString();
                var yStr = y.ToString();
                var xLength = xStr.Length;
                var yLength = yStr.Length;
                for (int i = 0; i < xLength; i++)
                for (int j = 0; j < yLength; j++)
                {
                    if (xStr[i] == yStr[i])
                        break;
                    if (j == xLength) return false;
                }

                return true;
            }

            public static Dictionary<char, long> dic = new Dictionary<char, long>
            {
                {'0', 0},
                {'1', 1},
                {'2', 16},
                {'3', 241},
                {'4', 3616},
                {'5', 54241},
                {'6', 813616},
                {'7', 12204241},
                {'8', 183063616},
                {'9', 2745954241}
            };

            static int SumHash = 0;
            static string Str;

            public int GetHashCode(long obj)
            {
                long sumHashs = 0;
                var str = obj.ToString();
                var strLength = str.Length;
                for (int i = 0; i < strLength; i++)
                    sumHashs += dic[str[i]];

                return (int) sumHashs;
            }
        }

        static void Main(string[] args)
        {
            //4366881 - 7
            //47111408 - 8
            //487875964 - 9
            var watch = new Stopwatch();
            watch.Start();

            var ss = GetCountsByCountDigit(9);
            Console.WriteLine(watch.Elapsed);
            Console.WriteLine(ss);
        }



        static ulong GetCountsByCountDigit(int countDigit)
        {
            var cache = new Dictionary<long, bool>(new LongComparer());
            ulong countNumbersThatСanBeDividedIntoTwoParts = 0;
            var left = 0;
            var right = Math.Pow(10, countDigit);
            for (var i = 0; i < right; i++)
            {
                if (!cache.TryGetValue(i, out var value))
                {
                    value = CanBeDividedInHalf(i);
                    cache.Add(i, value);
                }

                if (value) countNumbersThatСanBeDividedIntoTwoParts++;
            }

            return countNumbersThatСanBeDividedIntoTwoParts;
        }
    }
}