﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Circle
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public override bool Equals(object obj)
        {
            var point = obj as Point;
            return point.X == X && point.Y == Y;
        }

        public static bool operator ==(Point p, Point p1)
        {
            return Equals(p, p1);
        }

        public static bool operator !=(Point p, Point p1)
        {
            return !Equals(p, p1);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
            return base.GetHashCode();
        }
    }

    class Program
    {
        static Point GetCenterCircle(Point a, Point b, Point c)
        {
            var d = 2 * (a.X * (b.Y - c.Y) + b.X * (c.Y - a.Y) + c.X * (a.Y - b.Y));
            var aXaY = a.X * a.X + a.Y * a.Y;
            var bXbY = b.X * b.X + b.Y * b.Y;
            var cXcY = c.X * c.X + c.Y * c.Y;

            var point = new Point();
            point.X = (aXaY * (b.Y - c.Y) + bXbY * (c.Y - a.Y) + cXcY * (a.Y - b.Y)) / d;
            point.Y = (aXaY * (c.X - b.X) + bXbY * (a.X - c.X) + cXcY * (b.X - a.X)) / d;
            return point;
        }

        static double Distance(Point x, Point y)
        {
            var pow = Math.Pow(x.X - y.X, 2) + Math.Pow(x.Y - y.Y, 2);
            return Math.Sqrt(pow);
        }


        static bool IsTryangle(Point p, Point p1, Point p2)
        {
            return p != p1 && p != p2 && p1 != p2;
        }

        static (Point, double radius) GetCenterMoreRemoteDots(List<Point> points)
        {
            Point MoreRemoteDot1 = null;
            Point MoreRemoteDot2 = null;
            double maxDistance = 0;

            for (var i = 0; i < points.Count; i++)
            {
                for (var ii = i + 1; ii < points.Count; ii++)
                {
                    var distance = Distance(points[i], points[ii]);
                    if (!(distance > maxDistance)) continue;
                    maxDistance = distance;
                    MoreRemoteDot1 = points[i];
                    MoreRemoteDot2 = points[ii];
                }
            }

            var radius = Distance(MoreRemoteDot1, MoreRemoteDot2) / 2;
            var x = (MoreRemoteDot1.X + MoreRemoteDot2.X) / 2;
            var y = (MoreRemoteDot1.Y + MoreRemoteDot2.Y) / 2;
            return (new Point {X = x, Y = y}, radius);
        }

        static bool CircleBetweenMoreRemoteDotsContainsAllDotsByCenter(List<Point> points,
            (Point center, double radius) tuple)
        {
            var (center, radius) = tuple;
            for (int i = 0; i < points.Count; i++)
            {
                var distance = Distance(points[i], center);
                if (distance > radius) return false;
            }

            return true;
        }

        static Point GetCenterCircleContainsAllDots(List<Point> points)
        {
            List<(Point, Point, Point)> res = new List<(Point, Point, Point)>();
            for (int i = 0; i < points.Count; i++)
            for (int ii = i; ii < points.Count; ii++)
            for (int iii = ii; iii < points.Count; iii++)
                if (IsTryangle(points[i], points[ii], points[iii]))
                    res.Add((points[i], points[ii], points[iii]));

            for (int i = 0; i < res.Count; i++)
            {
                for (int ii = 0; ii < points.Count; ii++)
                {
                    var center = GetCenterCircle(res[i].Item1, res[i].Item2, res[i].Item3);
                    var radius = Distance(center, res[i].Item1);
                    if (CircleBetweenMoreRemoteDotsContainsAllDotsByCenter(points, (center, radius))) return center;
                }
            }

            throw new ArgumentException();
        }


        static int Main(string[] args)
        {
            var points = File.ReadAllLines($"{Directory.GetCurrentDirectory()}/input.txt").Select(x =>
            {
                var spl = x.Split(' ');
                return new Point {X = int.Parse(spl[0]), Y = int.Parse(spl[1])};
            }).ToList();


            var centerMoreRemoteDots = GetCenterMoreRemoteDots(points);
            if (CircleBetweenMoreRemoteDotsContainsAllDotsByCenter(points, centerMoreRemoteDots))
            {
                Print(centerMoreRemoteDots.Item1.X, centerMoreRemoteDots.Item1.Y);
                return 0;
            }

            var center = GetCenterCircleContainsAllDots(points);
            Print(center.X, center.Y);
            return 1;
        }

        static void Print(double X, double Y)
        {
            Console.WriteLine($"Центр окружности X:{X} Y: {Y}");
        }
    }
}