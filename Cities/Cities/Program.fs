﻿// Learn more about F# at http://fsharp.org

open System
open System.IO

let countElements a list = list |> List.filter (fun x -> x = a) |> List.length;
let lower (str: string) = str.ToLower();
let path = sprintf "%s/input.txt" (Directory.GetCurrentDirectory())

[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"
    let cities = File.ReadAllLines path |> Array.map lower;
    let (firstLetters, lastLetters) = cities |> Array.map (fun x -> ([ Seq.head x ], [ Seq.last x ]))
                                             |> Array.fold (fun acc (x, y) ->
                                                    let (arr1, arr2) = acc
                                                    (x @ arr1, y @ arr2))
                                                    (List.Empty, List.Empty)


    let chars = (firstLetters @ lastLetters) |> List.distinct
    let r = chars |> List.map (fun c -> (firstLetters |> countElements c) - (lastLetters |> countElements c))
    let notNullElements = r |> List.filter (fun x -> x <> 0);

    printf "%s" (if notNullElements
                        |> List.distinct
                        |> List.fold (fun acc cur -> acc && [ -1; 1 ] |> List.contains (cur)) true
                        then "OK"
                        else "Error")
    0 // return an integer exit code
