﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace average
{
    public static class NodeListExtensions
    {
        public static Node Get(this List<Node> nodes, int position) =>
            nodes.Where(x => x.Position <= position).OrderBy(x => x.Position).LastOrDefault();
    }

    public class AverageInfo
    {
        public double Sum { get; set; }
        public int Count { get; set; }
    }


    public class Node
    {
        public int Position { get; set; }
        public double Value { get; set; }

        public static bool operator !=(Node n1, double n2)
        {
            return !ValueEquals(n1, n2);
        }


        public static bool operator ==(Node n1, double n2)
        {
            return ValueEquals(n1, n2);
        }

        public static bool ValueEquals(Node n1, double n2) => n1.Value == n2;
    }

    class Program
    {
        static int[] FindPath(List<int> numbers, List<List<Node>> nodes, int x, int y)
        {
            var findPath = nodes[y].Get(x).Position;
            if (y == 1) return new[] {findPath};
            var upperValue = nodes[y - 1].Get(x);
            var value = nodes[y].Get(x).Value;
            return upperValue.Value == value
                ? FindPath(numbers, nodes, x, y - 1)
                : new[] {numbers[y]}.Concat(FindPath(numbers, nodes, x - numbers[y], y - 1)).ToArray();
        }

        static int[] GetNumbers(List<int> numbers, double sum)
        {
            var count = numbers.Count;

            var nodes = Enumerable.Repeat(1, count).Select(x => new List<Node>()).ToList();

            for (var i = 1; i < count; i++)
            for (var ii = numbers[i]; ii <= sum; ii++)
            {
                var upperValue = nodes[i - 1].Get(ii)?.Value ?? 0;
                var previousValue = PreviousValue(nodes, ii - 1)?.Value ?? 0;
                var altValue = ii - numbers[i] < 0
                    ? 0
                    : (NodePreviousRowValue(nodes, ii - numbers[i], i)?.Value ?? 0) + numbers[i];

                if (altValue > ii) altValue = 0;
                var value = Math.Max(upperValue, altValue);
                if (value != previousValue) nodes[i].Add(new Node {Position = ii, Value = value});
                if (value == sum) break;
            }


            var v = nodes[count - 1].Get((int) sum);
            return v != sum
                ? null
                : FindPath(numbers, nodes, (int) sum, count - 1);
        }

        static Node NodePreviousRowValue(List<List<Node>> list, int foundIndex, int currentRow)
        {
            return PreviousValue(Enumerable.Range(1, currentRow - 1).Select(x => list[x]).ToList(), foundIndex);
        }

        static Node PreviousValue(List<List<Node>> list, int foundIndex)
        {
            var s = new List<List<Node>>();
            foreach (var node in list) s.Add(node);
            s.Reverse();
            var flatList = s.Aggregate(new List<Node>(), (a, c) =>
            {
                foreach (var cItem in c)
                    if (a.All(x => x.Position != cItem.Position))
                        a.Add(cItem);

                return a;
            });
            return flatList.Get(foundIndex);
        }

        static void Main(string[] args)
        {
            var numbers = File.ReadAllLines("input.txt").Select(int.Parse).ToList();
            var count = numbers.Count;
            var sum = numbers.Sum();
            var averageInfos = Enumerable
                .Range(1, count / 2)
                .Select(x => new AverageInfo {Sum = (double) ((count - x) * sum / count), Count = x});
            var average = averageInfos
                .Select(x => GetNumbers(numbers.Prepend(0).ToList(), x.Sum)).FirstOrDefault(x => x != null);

            if (average == null)
            {
                Console.WriteLine("Нельзя");
                return;
            }

            Console.WriteLine("Первая группа");
            foreach (var t in average)
                Console.WriteLine(t);

            foreach (var t in average)
                numbers.Remove(t);

            Console.WriteLine("Вторая группа");

            foreach (var t in numbers)
                Console.WriteLine(t);
        }
    }
}