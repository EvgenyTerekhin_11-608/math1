﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Circle
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    class Program
    {
        static async Task<int> Main(string[] args)
        {
            var dic = new Dictionary<int, int>();

            var path = $"{Directory.GetCurrentDirectory()}/input.txt";
            int length;
            using (var file = new StreamReader(path))
            {
                length = int.Parse(await file.ReadLineAsync());
                for (int i = 0; i < length; i++)
                {
                    var spl = (await file.ReadLineAsync()).Split(' ');
                    var left = int.Parse(spl[0]);
                    var rigth = int.Parse(spl[1]);
//                    var (left, rigth) = (await file.ReadLineAsync())
//                        .Split(' ').Select(int.Parse)
//                        .Aggregate(new List<List<int>>(), (acc, curr) =>
//                        {
//                            List<int> createAndGet()
//                            {
//                                var item = new List<int>();
//                                acc.Add(item);
//                                return item;
//                            }
//
//                            (acc.Count == 0 ? createAndGet() : acc[0]).Add(curr);
//                            return acc;
//                        })
//                        .Select(x => (x[0], x[1]))
//                        .First();

                    if (!dic.TryAdd(left, 1))
                        dic[left]++;
                    if (!dic.TryAdd(rigth, -1))
                        dic[rigth]--;
                }
            }


            var notNull = false;

            var dicList = dic.ToList();
            for (int i = 0;
                i < dic.Count && !notNull;
                i++)
                if (dicList[i].Value != 0)
                    notNull = true;

            Console.WriteLine(notNull ? "NO" : "YES");
            return 1;
        }
    }
}